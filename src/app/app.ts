import { ReportBooklet } from "./classes/reportBooklet.class";

let appContainer = document.getElementById('app');
let fromKW = 1;
let toKW = 30;
let year = 2019;

for(var i = fromKW; i <= toKW; i++) {

    let reportBooklet = new ReportBooklet();
    let singleBooklet = document.createElement('div');
    singleBooklet.setAttribute('id', 'sb'+i);
    singleBooklet.setAttribute('class', 'bookletWrap');
    singleBooklet.innerHTML = reportBooklet.getWeekBookletMarkup(i, year);

    appContainer.appendChild(singleBooklet);

}
