export class DateUtility {

    public getDateOfWeek(_weekNumber: number, _year: number): Date {
        let simple = new Date(_year, 0, 1 + (_weekNumber - 1) * 7);
        let dow = simple.getDay();
        let ISOWeekStart = simple;
        if (dow <= 4)
            ISOWeekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOWeekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOWeekStart;
    }

    public getReadableDate(_date: Date) {
        let today: Date = _date;
        let dd: any = today.getDate();
        let mm: any = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return dd + '.' + mm + '.' + yyyy;
    }

}