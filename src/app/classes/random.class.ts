export class Randomice {

    public getRandomInt(_min: number, _max: number): number {
        return Math.floor(Math.random() * (_max - _min) + _min);
    }

}