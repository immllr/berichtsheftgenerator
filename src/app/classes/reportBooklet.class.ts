import "../../styles/components/booklet.scss";
import { Sentences } from "./sentences.class";
import { Randomice } from "./random.class";
import { DateUtility } from "./date.class";

export class ReportBooklet {

    private sentence = new Sentences();
    private randomice = new Randomice();

    public  getSentenceByWeekNumber(_weekNumber: number, _day: number, _department: string, _number: number) {


        var freeDays: any;

        let test: any = [];

        test["1-0"] = true;
        test["1-1"] = true;

        test["8-3"] = true;
        test["8-4"] = true;

        test["16-4"] = true;

        test["18-0"] = true;
        test["18-1"] = true;
        test["18-2"] = true;
        test["18-3"] = true;
        test["18-4"] = true;

        var index = _weekNumber + "-" + _day;




        if(test[index] !== undefined) {
            return "Frei.";
        } else {
            switch (_department) {
                case "support":
                    return this.sentence.getSentence(_department, _number);
                    break;
                case "design":
                    return this.sentence.getSentence(_department, _number);
                    break;
                case "school":
                    return this.sentence.getSentence(_department, _number);
                    break;
                default:
                    return false;
                    break;
            }
        }

    }

    public getWeekBookletMarkup(_weekNumber: number, _year: number): string {

        let dateUtility = new DateUtility();
        let startDate = dateUtility.getDateOfWeek(_weekNumber, _year);
        let endDate: Date = new Date(startDate.getTime()+(4*24*60*60*1000));



        return '<div class="container">' +
            '<h1>Berichtsheft</h1>' +
            '<span class="kwSpan">KW'+_weekNumber+' // '+dateUtility.getReadableDate(startDate)+' - '+dateUtility.getReadableDate(endDate)+'</span>' +
            '<table class="booklet" cellpadding="0" cellspacing="0"><tbody>' +
            '<tr class="head"><td><span class="booklet__rotate">Montag</span></td></tr><tr><td>'+this.getSentenceByWeekNumber(_weekNumber, 0, 'support', this.randomice.getRandomInt(0, this.sentence.getSupportLength()))+'</td></tr>' +
            '<tr class="head"><td><span class="booklet__rotate">Dienstag</span></td></tr><tr><td>'+this.getSentenceByWeekNumber(_weekNumber, 1, 'support', this.randomice.getRandomInt(0, this.sentence.getSupportLength()))+'</td></tr>' +
            '<tr class="head"><td><span class="booklet__rotate">Mittwoch</span></td></tr><tr><td>'+this.getSentenceByWeekNumber(_weekNumber, 2, 'school', this.randomice.getRandomInt(0, this.sentence.getSchoolLength()))+'</td></tr>' +
            '<tr class="head"><td><span class="booklet__rotate">Donnerstag</span></td></tr><tr><td>'+this.getSentenceByWeekNumber(_weekNumber, 3, 'design', this.randomice.getRandomInt(0, this.sentence.getDesignLength()))+'</td></tr>' +
            '<tr class="head"><td><span class="booklet__rotate">Freitag</span></td></tr><tr><td>'+this.getSentenceByWeekNumber(_weekNumber, 4, 'design', this.randomice.getRandomInt(0, this.sentence.getDesignLength()))+'</td></tr>' +
            '</tbody></table><div class="signature"><span class="title">Unterschrift des Ausbilders</span></div><div class="signature"><span class="title">Unterschrift des Auszubildenden</span></div></div>'
    }
}