export class Sentences {

    private sentencesSupport = [
      "Heute habe ich in der Supportabteilung einige Bugs und Features umgesetzt. Genutzte Software: JetBrains PhpStorm, Adobe Photoshop und Google Chrome",
      "In der Supportabteilung habe ich heute einige Tasks aus der Entwickler-Queue umgesetzt. Genutzte Software: JetBrains PhpStorm, Adobe Photoshop und Google Chrome",
      "Im Support habe ich heute diverse kleinere Aufgaben aus dem FE & BE Bereich umgesetzt. Genutzte Software: JetBrains PhpStorm und Google Chrome",
      "Heute im Support habe ich Front-End Tasks für einige Kunden umgesetzt und Bugs behoben. Genutzte Software: JetBrains PhpStorm und Google Chrome",
      "Heute habe ich Tasks aus der Entwickler-Queue umgesetzt in der Supportabteilung. Darunter Bugs und Features. Genutzte Software: JetBrains PhpStorm und Google Chrome.",
      "In der Supportabteilung habe ich heute diverse Queue-Tickets abgearbeitet und weitere Tickets bearbeitet. Genutzte Software: JetBrains PhpStorm & Google Chrome"
    ];
    private sentencesDesign = [
        "Heute habe ich in der Grafikabteiltung Layouts für Kunden erstellt und einen Grafikcheck durchgeführt. Genutzte Software war Axure, Illustrator und Photoshop",
        "In der Grafik habe ich heute neben der Erstellung diverser Designs auch Präsentations-Folien gestaltet. Genutzte Software war Illustrator, Sketch und Axure",
        "Heute habe ich verschiedene PDFs gestaltet und Designs für unsere Kunden erstellt. Genutzte Software war Adobe Illustrator, Sketch und Adobe InDesign.",
        "Neben der Gestaltung von diversen Seiten/Plus-Modulen habe ich noch für bestimmte Websites einen Grafikcheck durchgeführt. Genutzte Software: Illustrator, Axure, Google Chrome",
        "In der Grafikabteilung habe ich heute für einen Kunden eine Startseite gestaltet nach deren Corporate Design. Als Software habe ich Illustrator, Photoshop, Sketch & Axure genutzt.",
        "Hier steht ein Design Satz 6",
        "Hier steht ein Design Satz 7",
        "Hier steht ein Design Satz 8",
        "Hier steht ein Design Satz 9",
        "Hier steht ein Design Satz 10",
        "Hier steht ein Design Satz 11",
        "Hier steht ein Design Satz 12",
        "Hier steht ein Design Satz 13",
        "Hier steht ein Design Satz 14",
        "Hier steht ein Design Satz 15"
    ];
    private sentencesSchool = [
        "Schule"
    ];

    /**
     *
     * @param _department
     * @param _index
     */
    public getSentence(_department: string, _index: number) {
        switch (_department) {
            case "support":
                return this.sentencesSupport[_index];
                break;
            case "design":
                return this.sentencesDesign[_index];
                break;
            case "school":
                return this.sentencesSchool[_index];
                break;
            default:
                return false;
                break;
        }
    }

    public getSupportLength(): number {
        return this.sentencesSupport.length;
    }

    public getDesignLength(): number {
        return this.sentencesDesign.length;
    }

    public getSchoolLength(): number {
        return this.sentencesSupport.length;
    }

}